import json
import requests

from signedjson.key import generate_signing_key, get_verify_key, read_signing_keys
from signedjson.sign import (
    sign_json, verify_signed_json, SignatureVerifyException
)


# curl https://libera.chat/.well-known/matrix/server
# curl 'https://libera.ems.host/_matrix/key/v2/server'

def authorization_headers(origin_name, origin_signing_key,
                          destination_name, request_method, request_target,
                          content=None):
    request_json = {
         "method": request_method,
         "uri": request_target,
         "origin": origin_name,
         "destination": destination_name,
    }

    if content is not None:
        # Assuming content is already parsed as JSON
        request_json["content"] = content

    signed_json = sign_json(request_json, origin_name, origin_signing_key)

    authorization_headers = []

    for key, sig in signed_json["signatures"][origin_name].items():
        authorization_headers.append(bytes(
            "X-Matrix origin=\"%s\",destination=\"%s\",key=\"%s\",sig=\"%s\"" % (
                origin_name, destination_name, key, sig),
            encoding="utf-8"))

    return ("Authorization", authorization_headers[0])

sk = None
with open('homeserver.signing_key', 'r') as k:
  sk = read_signing_keys(k)[0]

head_key, head_val = authorization_headers("deuxfleurs.fr", sk, "libera.chat", "GET", "/_matrix/federation/unstable/rooms/!FmBSWmHvlUJtFiEjrH:libera.chat/complexity")
r = requests.get('https://libera.ems.host:443/_matrix/federation/unstable/rooms/!FmBSWmHvlUJtFiEjrH:libera.chat/complexity', headers = { head_key: head_val })
print(r.json())

